# vim: set fileencoding=utf-8

alias :λ :lambda
π = Math::PI

require 'bundler/setup'
Bundler.require(:default)

require 'yaml'
require 'json'
require 'mongo'
require 'sinatra'

case ENV['RACK_ENV'].to_sym
when :development
  require 'sinatra/reloader'
  require 'better_errors'
  require 'binding_of_caller'

  use BetterErrors::Middleware
  BetterErrors.application_root = File.expand_path('..', __FILE__)
end

configure :development do
  enable :raise_errors
end

configure do
  enable :clean_trace, :inline_templates, :static, :session
  disable :show_exceptions # use better_errors instead

  set :haml, format: :html5
end

config_file File.dirname(__FILE__) + '/config.yaml'

require File.dirname(__FILE__) + '/db.rb'
require File.dirname(__FILE__) + '/app.rb'

run Sinatra::Application

