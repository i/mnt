# vim: set fileencoding=utf-8

helpers do
  def title(custom_title = nil)
    prefix = custom_title ? "#{custom_title} – " : ""
    "#{prefix}#{settings.title}"
  end

  def config
    redirect_uri = 'http://mnt.so' + settings.login_page
    scopes = ["email"]
    {
      logins: {
        fb: {
          url: "https://www.facebook.com/dialog/oauth?" +
               "client_id=" + ENV['FACEBOOK_APP_ID'].to_s +
               "&scope=" + scopes.join(",") +
               "&response_type=token" +
               "&redirect_uri=" + redirect_uri,
          img: "/images/facebook_280x280.png",
          alt: "Facebook Login",
          hint: "Click here to log in using Facebook"
        }
      }
    }
  end
end

get '/favicon.ico' do
  204
end

get '/' do
  "Welcome to <strong>#{settings.title}</strong>!"
end

get '/crash' do
  raise 'App crased'
end

get settings.login_page do
  haml :login, locals: {
    title: title('Log in'),
    style: <<-CSS,
body {
	text-align:center;
	font-family:'lucida grande',tahoma,verdana,arial,sans-serif;
	font-size:12px;
}
#js_required {
	margin: 8px;
	padding: 4px;
	background-color: rgb(60, 95, 175);
	border: 1px solid rgb(130, 150, 200);
	color: white;
	font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;
}
#bg {
	width:100%;
	height:100%;
	position:absolute;

	z-index:-1;
}
#layout {
	z-index:15;

	width:160px;
	height:160px;
	position:absolute;
	top:50%;
	left:50%;
	margin:-80px 0 0 -80px;

	text-align:center;
}
CSS
    js: ['/js/cross.js', '/js/login.js']
  }
end

get settings.login_page + '.json' do
  content_type :js
  "var CONFIG = #{JSON.pretty_generate(config)};"
end

get '/:short' do
  params[:short].to_s
end


get '/_internals/auth/facebook/extend' do
  redirect to(settings.login_page) unless params[:token]
  url = 'https://graph.facebook.com/oauth/access_token?' +
        'client_id=' + ENV['FACEBOOK_APP_ID'].to_s + '&' +
        'client_secret=' + ENV['FACEBOOK_APP_SECRET'].to_s + '&' +
        'grant_type=fb_exchange_token&fb_exchange_token=' + params[:token]
  redirect to(settings.login_page + '#' + Faraday.get(url).env[:body])
end

