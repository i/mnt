(function (window, document, undefined) {

	var req,
	    layout = $$("layout"),
	    throbber = new Image,
	    token = params("access_token"),
	    return_to = params("return_to");

	throbber.alt = "… loading …";
	throbber.src = "https://images-na.ssl-images-amazon.com/images/G/01/webservices/console/loading-blue-large.gif";
	// throbber.src = "/images/loading-blue-large.gif";

	if (return_to && isSessionStorageAvailable()) {
		sessionStorage.setItem("return_to", return_to);
	}

	if (token) {
		layout.appendChild(throbber);

		if (params("expires_in") && isSessionStorageAvailable() && sessionStorage.getItem("remember_me")) {
			location.replace("/_internals/auth/facebook/extend?token=" + token);
			return;
		}

		req = new Request;
		req.open("POST", "/api/v1/auth/facebook");
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.setRequestHeader("Accept", "application/json;q=1.0");
		req.onreadystatechange = function () {
			var json, p, return_to;
			if (4 === req.readyState) {
				json = JSON.parse(req.responseText);
				if (json && json.user_id) {
					// alert("Logged as user #" + json.user_id);
					location.replace(
						isSessionStorageAvailable()
						  && (return_to = sessionStorage.getItem("return_to"))
						    ? return_to : "/"
					);
				} else {
					if (json.error) {
						if (190 === json.error.code && 463 === json.error.error_subcode) {
/* {
 * "error":
 * 	{
 * 		"message":"Error validating access token: Session has expired at unix time 1355745600. The current unix time is 1355760300.",
 * 		"type":"OAuthException",
 * 		"code":190,
 * 		"error_subcode":463
 * 	}
 * } */
/* {
 * "error":
 * {
 * 		"message":"Error validating access token: Session is invalid. This could be because the application was uninstalled after the session was created.",
 * 		"type":"OAuthException",
 * 		"code":190,
 * 		"error_subcode":461
 * 	}
 * } */
							location.replace("/login.html");
							return;
						}
						while (layout.hasChildNodes()) {
							layout.removeChild(layout.firstChild);
						}
						p = new Node("p", { style: "text-align:justify;" });
						p.appendChild(document.createTextNode(json.error.message));
						layout.appendChild(p);
					}
					alert("Error:\n\n" + req.responseText);
				}
			}
		};
		req.send("provider_token=" + token);

		return;
	}
	each_login(function (provider) {
		var img, a, url = this.url;
		img = new Image;
		img.src = this.img;
		if (this.alt) {
			img.alt = this.alt;
		}
		img.width = 128;
		img.height = 128;
		a = new Node("a", { href: url, title: this.hint });
		a.onclick = function () {
			var that = this;
			each_login(function () { this.style.display = "none"; });
			layout.appendChild(throbber);
			if (throbber.complete) {
				handle();
			} else {
				throbber.onload = handle;
			}
			return false;

			function handle () { location.replace(url); }
		}
		a.appendChild(img);
		$(img).hide().load(function () { $(this).fadeIn(1400); } );
		CONFIG.logins[provider] = img;
		layout.appendChild(a);
	});

	function each_login (cb) {
		var key, logins = CONFIG.logins;
		if ("function" !== typeof cb) {
			return;
		}
		for (key in logins) {
			if (logins.hasOwnProperty(key)) {
				cb.call(logins[key], key);
			}
		}
	}

	function isSessionStorageAvailable () {
		try {
			return 'sessionStorage' in window && window['sessionStorage'] !== null;
		} catch (e) {
			return false;
		}
	}

})(window, document);

