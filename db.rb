helpers do
  def connection
    @connection ||= begin
      if production?
        Mongo::Connection.new(ENV['OPENSHIFT_MONGODB_DB_HOST'], ENV['OPENSHIFT_MONGODB_DB_PORT'])
      else
        Mongo::Connection.new
      end
    end
  end

  def db
    return @db if @db
    @db = connection[production? ? ENV['OPENSHIFT_APP_NAME'] : 'mnt']
    @db.authenticate(
      ENV['OPENSHIFT_MONGODB_DB_USERNAME'],
      ENV['OPENSHIFT_MONGODB_DB_PASSWORD']) if production?
    @db
  end
end

